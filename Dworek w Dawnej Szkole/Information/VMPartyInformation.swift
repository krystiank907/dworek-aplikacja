//
//  VMPartyInformation.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 28.03.2018.
//  Copyright © 2018 Krystian Kulawiak. All rights reserved.
//

import Foundation
import RealmSwift

class VMPartyInforamtion: CurrentClientProtocol{
    
    var clientID: String
    var dateData: PartyData
    let clientData: ClientData
    
    init(clientID:String, dateData: PartyData, clientData: ClientData){
        self.clientID = clientID
        self.dateData = dateData
        self.clientData = clientData
    }

}
