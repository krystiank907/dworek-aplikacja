//
//  InformationViewController.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 05.07.2017.
//  Copyright © 2017 Krystian Kulawiak. All rights reserved.
//

import UIKit
import RealmSwift

class InformationViewController: CustomUIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var party: UITextField!
    @IBOutlet weak var startTime: UITextField!
    @IBOutlet weak var music: UITextField!
    @IBOutlet weak var amountChild: UITextField!
    @IBOutlet weak var amountPerson: UITextField!
    @IBOutlet weak var otherInformation: UITextView!

    var viewModel:VMPartyInforamtion!
    
    
    required init(coder decoder: NSCoder) {
        super.init(coder: decoder)!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.delegate = self
    }
    
    
    init(viewModel:VMPartyInforamtion){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil )
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBar()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @objc func nextPage(){
        save()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "menu") as UIViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    func setNavigationBar() {
        navigationItem.title = "Informacje"
        let nextItem = UIBarButtonItem(title: "Dalej", style: .plain, target: self, action: #selector(nextPage))
        navigationItem.rightBarButtonItem = nextItem
    }
        
    @objc func keyboardWillShow(notification:NSNotification){
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInset
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let nextTage=textField.tag+1;
        // Try to find next responder
        let nextResponder=textField.superview?.viewWithTag(nextTage) as UIResponder?
        
        if (nextResponder != nil){
            // Found next responder, so set it.
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            // Not found, so remove keyboard
            textField.resignFirstResponder()
        }
        return false // We do not want UITextField to insert line-breaks.
    }
    
    func save(){
        let syncConfig = SyncConfiguration(user: SyncUser.current!, realmURL: Constants.REALM_URL)
        let realm: Realm = try! Realm(configuration: Realm.Configuration(syncConfiguration: syncConfig))
        let partyData = PartyInfoData()
        partyData.party = party.text!
        partyData.startTime = startTime.text!
        partyData.music = music.text!
        partyData.amountChild = amountChild.text!
        partyData.amountPerson = amountPerson.text!
        partyData.otherInformation = otherInformation.text!
        try! realm.write {
            realm.add(partyData, update: true)
            viewModel.dateData.partyInfoData.append(partyData)
        }
        realm.refresh()
    }


}
