//
//  StartViewController.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 04.07.2017.
//  Copyright © 2017 Krystian Kulawiak. All rights reserved.
//

import UIKit
import RealmSwift


class StartViewController: CustomUIViewController{
    

    @IBOutlet weak var StartImage: UIImageView!
    @IBOutlet weak var calendarIcon: UIImageView!
    @IBOutlet weak var clientListIcon: UIImageView!
    
    let clientId = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let startTapGesture = UITapGestureRecognizer(target: self, action: #selector(StartViewController.goToClient(gesture:)))
        let calendarTapGesture = UITapGestureRecognizer(target: self, action: #selector(StartViewController.goToCalendar(gesture:)))
        let clientListTapGesture = UITapGestureRecognizer(target: self, action: #selector(StartViewController.goToClientList(gesture:)))
        self.StartImage.addGestureRecognizer(startTapGesture)
        self.calendarIcon.addGestureRecognizer(calendarTapGesture)
        self.clientListIcon.addGestureRecognizer(clientListTapGesture)
        clientListIcon.isUserInteractionEnabled = true
        calendarIcon.isUserInteractionEnabled = true
        StartImage.isUserInteractionEnabled = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func goToClient(gesture: UITapGestureRecognizer)
    {
        if (gesture.view as? UIImageView) != nil {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "client") as! ClientViewController
            vc.viewModel = VMClient(clientID: clientId,isDetailClient: false)
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    @objc func goToCalendar(gesture: UITapGestureRecognizer){
        if (gesture.view as? UIImageView) != nil {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "calendar") as! CalendarViewController
            vc.viewModel = VMCalendar(clientID: "", clientData: ClientData(), isPickDate: false)
            navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    @objc func goToClientList(gesture: UITapGestureRecognizer){
        if (gesture.view as? UIImageView) != nil {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "clientList") as! ClientListViewController
            vc.viewModel = VMClientList()
            navigationController?.pushViewController(vc, animated: true)

        }
    }

}
