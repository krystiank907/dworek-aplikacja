//
//  VMClient.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 25.03.2018.
//  Copyright © 2018 Krystian Kulawiak. All rights reserved.
//

import Foundation
import RealmSwift

class VMClient: CurrentClientProtocol{
    var clientID: String
    var isDetailClient: Bool
    let items: Results<ClientData>
    var isReadClient:Bool
    
    init(clientID:String, isDetailClient:Bool, isReadClient:Bool = false){
        let realm = try! Realm()
        self.items = realm.objects(ClientData.self).sorted(byKeyPath: "name", ascending: false)
        self.clientID = clientID
        self.isDetailClient = isDetailClient
        self.isReadClient = isReadClient
        realm.refresh()
    }
    func delateUserAction(){
        let config = Realm.Configuration(syncConfiguration: SyncConfiguration(user: SyncUser.current!, realmURL: Constants.REALM_URL))
        let realm = try! Realm(configuration: config)
        let item = ClientData.currentClient(clientId: clientID)
        try! realm.write {
            realm.delete(item!)
        }
    }
    
}
