//
//  VMClientDetails.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 18.04.2018.
//  Copyright © 2018 Krystian Kulawiak. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class VMClientDetails{
    enum Section: Int {
        enum Field: Int {
            case name
            case lastName
            case city
            case zipcode
            case street
            case homeNumber
            case phoneNumb
            case phoneNumbSecond
            case mail
            
            var placeHolder: String {
                switch self {
                case .name:
                    return "Imię"
                case .lastName:
                    return "Nazwisko"
                case .city:
                    return "Miejscowość"
                case .zipcode:
                    return "Kod Pocztowy"
                case .street:
                    return "Ulica"
                case .homeNumber:
                    return "Numer mieszkania"
                case .phoneNumb:
                    return "Numer telefomu"
                case .phoneNumbSecond:
                    return "Drugi numer telefonu"
                case .mail:
                    return "Adres Mail"
                }
            }
        }
        
        case name = 0, address, phone
        
        static var count: Int {
            return Section.phone.rawValue + 1
        }
        
        var fields: [Field] {
            switch self {
            case .name:
                return [.name, .lastName]
            case .address:
                return [.city, .zipcode, .street, .homeNumber]
            case .phone:
                return [.phoneNumb, .phoneNumbSecond, .mail]
            }
        }
    }
    
    var clientID: String
    let items: Results<ClientData>
    
    init(clientID:String){
        let realm = try! Realm()
        self.items = realm.objects(ClientData.self).sorted(byKeyPath: "name", ascending: false)
        self.clientID = clientID
        realm.refresh()
    }
    func delateUserAction(){
        let config = Realm.Configuration(syncConfiguration: SyncConfiguration(user: SyncUser.current!, realmURL: Constants.REALM_URL))
        let realm = try! Realm(configuration: config)
        let item = ClientData.currentClient(clientId: clientID)
        try! realm.write {
            realm.delete(item!)
        }
    }
    
    func fieldsForSection(_ section: Int) -> [Section.Field] {
        if let section = Section(rawValue: section) {
            let array = section.fields
            
            return array
        }
        
        return []
    }
    func numberOfRowsInSection(_ section: Int) -> Int {
        return fieldsForSection(section).count
        
    }
    
    var numberOfSections: Int {
        return Section.count
    }
    
    func fieldForRow(atIndexPath indexPath: IndexPath) -> Section.Field? {
        return fieldsForSection(indexPath.section)[indexPath.row]
    }
}
