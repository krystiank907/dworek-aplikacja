//
//  ClientListCell.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 25.03.2018.
//  Copyright © 2018 Krystian Kulawiak. All rights reserved.
//

import UIKit
import SwipeCellKit

class ClientListCell: SwipeTableViewCell {
    
    @IBOutlet var nameSurnameLabel: UILabel!
    @IBOutlet var phoneLabel: UILabel!
    @IBOutlet var nextPartyLabel: UILabel!
    
}
