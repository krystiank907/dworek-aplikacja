//
//  HeaderTableViewCell.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 19.04.2018.
//  Copyright © 2018 Krystian Kulawiak. All rights reserved.
//

import Foundation
import UIKit

class HeaderTableViewCell: UITableViewCell{
    
    @IBOutlet weak var textField: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textField.textColor = .white
        separatorView.backgroundColor = .white
        textField.backgroundColor = UIColor(hexString: "47AAB8")
        contentView.backgroundColor  = UIColor(hexString: "47AAB8")
        backgroundColor  = UIColor(hexString: "47AAB8")
    }
    
    func setupCell(text:String , image:String){
        textField.text = text
        iconImageView.image = UIImage(named: image)
    }
}
