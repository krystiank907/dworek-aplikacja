//
//  ClientListViewController.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 25.03.2018.
//  Copyright © 2018 Krystian Kulawiak. All rights reserved.
//

import UIKit
import RealmSwift
import SwipeCellKit
import RealmSearchViewController
import ChameleonFramework

class ClientListViewController: CustomUIViewController{
    
    @IBOutlet var clientListTableView: UITableView!
    var searchController = SearchBar(frame: CGRect.zero)
    var searchString = ""
    var searchControllerInUse = false
    
    let realm: Realm
    var items: Results<ClientData>
    var notificationToken: NotificationToken?
    var viewModel: VMClientList!
    
    required init?(coder aDecoder: NSCoder) {
        let syncConfig = SyncConfiguration(user: SyncUser.current!, realmURL: Constants.REALM_URL)
        self.realm = try! Realm(configuration: Realm.Configuration(syncConfiguration: syncConfig))
        self.items = realm.objects(ClientData.self).sorted(byKeyPath: "name", ascending: false)
        super.init(coder: aDecoder)
    }
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)  {
        let syncConfig = SyncConfiguration(user: SyncUser.current!, realmURL: Constants.REALM_URL)
        self.realm = try! Realm(configuration: Realm.Configuration(syncConfiguration: syncConfig))
        self.items = realm.objects(ClientData.self).sorted(byKeyPath: "name", ascending: false)
        super.init(nibName: nil, bundle: nil)
    }
    convenience init(viewModel: VMClientList){
        self.init()
        self.viewModel = viewModel
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.delegate = self
        searchString = ""
        searchInDataBase(searchString: searchString)
        searchControllerInUse = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar()
        clientListTableView.tableFooterView = UIView()
        notificationToken = items.observe { [weak self] (changes) in
            guard let tableView = self?.clientListTableView else { return }
            switch changes {
            case .initial:
                // Results are now populated and can be accessed without blocking the UI
                tableView.reloadData()
            case .update(_, let deletions, let insertions, let modifications):
                // Query results have changed, so apply them to the UITableView
                tableView.beginUpdates()
                tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}),
                                     with: .automatic)
                tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.endUpdates()
            case .error(let error):
                // An error occurred while opening the Realm file on the background worker thread
                fatalError("\(error)")
            }
            let nib = UINib(nibName: "EmptySearchTableViewCell", bundle: nil)
            
            self?.clientListTableView.register(nib, forCellReuseIdentifier: "EmptySearchTableViewCellIdentifier")
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func addAction(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "client") as! ClientViewController
        vc.viewModel = VMClient(clientID: "" ,isDetailClient: true)
        navigationController?.pushViewController(vc, animated: true)
    }
    @objc func searchAction(){
        setupSearchBar()
    }
    
    func setNavigationBar() {
        let plusItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.add, target: self, action: #selector(addAction))
        let searchItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.search, target: self, action: #selector(searchAction))
        navigationItem.title = "Lista Klientów"
        navigationItem.rightBarButtonItems = [plusItem, searchItem]
    }

    
}
extension ClientListViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchControllerInUse && searchString != "" && items.count == 0{
            return 1
        }
        return items.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if searchControllerInUse && searchString != "" && items.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "EmptySearchTableViewCellIdentifier") as! EmptySearchTableViewCell
            cell.setupCell(searchString)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "clientCell") as! ClientListCell
            let item = items[indexPath.row]
            cell.delegate = self
            cell.nameSurnameLabel.text = item.name + " " + item.surname
            cell.phoneLabel.text = item.phoneNumb
            cell.nextPartyLabel.text = item.city
            return cell
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchBarCancelButtonClicked(searchController)
        if items.count != 0{
            let item = items[indexPath.row]
            if viewModel.isReadCleint{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "client") as! ClientViewController
                vc.viewModel = VMClient(clientID: item.clientId,isDetailClient: false, isReadClient: true)
                let transition = CATransition()
                transition.duration = 0.3
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                transition.type = kCATransitionFromRight
                self.navigationController?.view.layer.add(transition, forKey: nil)
                let root = storyboard.instantiateViewController(withIdentifier: "start") as UIViewController
                navigationController?.setViewControllers([root,vc], animated: false)
            } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "clientDetail") as! ClientDetailViewController
                vc.viewModel = VMClientDetails(clientID: item.clientId)
                navigationController?.pushViewController(vc, animated: true)
            }
            tableView.deselectRow(at: indexPath, animated: false)
        }
    }
}

extension ClientListViewController: SwipeTableViewCellDelegate {
    
    func tableView(_ tableView  : UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        if !searchControllerInUse && searchString == "" && items.count != 0{
            let deleteAction = SwipeAction(style: .destructive, title: "Usuń") { [unowned self] (swipeAction, indexPath) in
                let alertController = UIAlertController(title: "Czy napewno chcesz usunąć użytkownika?", message: "", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Usuń", style: .default, handler: { [unowned self]
                    alert -> Void in
                    self.viewModel.delateUserAction(index: indexPath.row)
                }))
                alertController.addAction(UIAlertAction(title: "Nie usuwaj", style: .cancel, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
            return [deleteAction]
        }
        return nil
    }
}

extension ClientListViewController: UISearchBarDelegate, UISearchControllerDelegate{
    
    func setupSearchBar(){
        let cancelButton = UIBarButtonItem(title: "Anuluj", style: .plain, target: self, action: #selector(searchBarCancelButtonClicked))
        searchControllerInUse = true
        searchController.delegate = self
        searchController.text = nil
        searchController.placeholder = " Wpisz frazę"
        searchController.backgroundColor = .clear
        definesPresentationContext = true
        let searchBarContainer = SearchBarContainerView(customSearchBar: searchController)
        searchBarContainer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 40)
        navigationItem.titleView = searchBarContainer
        navigationItem.setRightBarButtonItems([cancelButton], animated: true)
        
        DispatchQueue.main.async {
            self.searchController.becomeFirstResponder()
        }
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        navigationItem.titleView = nil
        setNavigationBar()
        searchControllerInUse = false
        return true
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchString = searchBar.text!
        searchInDataBase(searchString: searchString)
        searchControllerInUse = true
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        navigationItem.titleView = nil
        setNavigationBar()
        searchControllerInUse = false
    }
    
    func searchInDataBase(searchString: String){
        if searchString == ""{
            self.items = realm.objects(ClientData.self).sorted(byKeyPath: "name", ascending: false)
        }else{
            let predicate = ClientData.searchText(searchString: searchString)
            self.items = realm.objects(ClientData.self).filter(predicate)
        }
        clientListTableView.reloadData()
        
    }
    
}
