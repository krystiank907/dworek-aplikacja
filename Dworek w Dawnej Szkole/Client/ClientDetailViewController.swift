//
//  ClientDetailViewController.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 18.04.2018.
//  Copyright © 2018 Krystian Kulawiak. All rights reserved.
//

import UIKit
import RealmSwift
import SwipeCellKit
import RealmSearchViewController
import ChameleonFramework


class ClientDetailViewController: CustomUIViewController{
    
    
    @IBOutlet var clientListTableView: UITableView!
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var surname: UITextField!
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var postCode: UITextField!
    @IBOutlet weak var street: UITextField!
    @IBOutlet weak var homeNumber: UITextField!
    @IBOutlet weak var phoneNumb: UITextField!
    @IBOutlet weak var phoneNumb2: UITextField!
    @IBOutlet weak var mail: UITextField!
    
    var viewModel: VMClientDetails!
    
    required init(coder decoder: NSCoder) {
        super.init(coder: decoder)!
    }
    
    init(viewModel:VMClientDetails){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil )
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar()
        let nib = UINib(nibName: "UserTextTableViewCell", bundle: nil)
        self.clientListTableView.register(nib, forCellReuseIdentifier: "UserTextTableViewCellIdentifier")
        let nextNib = UINib(nibName: "HeaderTableViewCell", bundle: nil)
        self.clientListTableView.register(nextNib, forCellReuseIdentifier: "HeaderTableViewCellIdentifier")
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setNavigationBar() {
        navigationItem.title = "Szczegóły Klienta"
        let finishItem = UIBarButtonItem(title: "Zapisz", style: .plain, target: self, action: #selector(finish))
        let delateItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.trash, target: self, action: #selector(delateAction))
        let rightItem = [finishItem,delateItem]
        navigationItem.rightBarButtonItems = rightItem
    }
    
    @objc func finish(){
        let isEmpty = isAllTextEmpty()
        if !isEmpty{
            save()
            view.endEditing(true)
            navigationController?.popViewController(animated: true)
        }else {
            emptyFiledAction()
        }
    }
    func save(){
        let config = Realm.Configuration(syncConfiguration: SyncConfiguration(user: SyncUser.current!, realmURL: Constants.REALM_URL))
        let realm = try! Realm(configuration: config)
        let client = ClientData()
        client.name = name.text ?? ""
        client.surname = surname.text ?? ""
        client.city = city.text ?? ""
        client.postCode = postCode.text ?? ""
        client.street = street.text ?? ""
        client.homeNumber = homeNumber.text ?? ""
        client.phoneNumb = phoneNumb.text ?? ""
        client.phoneNumb2 = phoneNumb2.text ?? ""
        client.clientId = viewModel.clientID
        try! realm.write {
            realm.add(client, update: true)
        }
    }
    
    @objc func delateAction(){
        let alertController = UIAlertController(title: "Czy napewno chcesz usunąć użytkownika?", message: "", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Usuń", style: .default, handler: { [unowned self]
            alert -> Void in
            self.viewModel.delateUserAction()
            self.navigationController?.popViewController(animated: true)
        }))
        alertController.addAction(UIAlertAction(title: "Nie usuwaj", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
}
extension ClientDetailViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection(section)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let sectionCell = tableView.dequeueReusableCell(withIdentifier: "UserTextTableViewCellIdentifier") as! UserTextTableViewCell
            let field = viewModel.fieldForRow(atIndexPath: indexPath)
            sectionCell.textField.placeholder = field?.placeHolder
            sectionCell.selectionStyle = .none
            let item = ClientData.currentClient(clientId: viewModel.clientID)
            switch field {
            case .name?:
                sectionCell.textField.text = item!.name
                self.name = sectionCell.textField
                sectionCell.separatorView.isHidden = false
                
            case .lastName?:
                sectionCell.textField.text = item!.surname
                self.surname = sectionCell.textField
                sectionCell.separatorView.isHidden = true
                
            case .city?:
                sectionCell.textField.text = item!.city
                self.city = sectionCell.textField
                sectionCell.separatorView.isHidden = false

            case .zipcode?:
                sectionCell.textField.text = item!.postCode
                self.postCode = sectionCell.textField
                sectionCell.separatorView.isHidden = false
                
            case .street?:
                sectionCell.textField.text = item!.street
                self.street = sectionCell.textField
                sectionCell.separatorView.isHidden = false
                
            case .homeNumber?:
                sectionCell.textField.text = item!.homeNumber
                self.homeNumber = sectionCell.textField
                sectionCell.separatorView.isHidden = true
                
            case .phoneNumb?:
                sectionCell.textField.text = item!.phoneNumb
                self.phoneNumb = sectionCell.textField
                sectionCell.separatorView.isHidden = false
                
            case .phoneNumbSecond?:
                sectionCell.textField.text = item!.phoneNumb2
                self.phoneNumb2 = sectionCell.textField
                sectionCell.separatorView.isHidden = false
                
            case .mail?:
                sectionCell.textField.text = item!.mail
                self.mail = sectionCell.textField
                sectionCell.separatorView.isHidden = false
            case .none:
                break
        }
            return sectionCell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard(_:)))
        view.addGestureRecognizer(tapGesture)
        view.backgroundColor = .clear
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 2{
            return 1
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCellIdentifier") as! HeaderTableViewCell
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard(_:)))
        cell.addGestureRecognizer(tapGesture)
        cell.backgroundColor = .clear
        if section == 0{
            cell.setupCell(text: "Imię i nazwisko", image: "boss")
        }
        if section == 1{
            cell.setupCell(text: "Adres zamieszkania", image: "address")
        }
        if section == 2{
            cell.setupCell(text: "Kontakt", image: "contact")
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 || section == 1 || section == 2{
            return 60
        }
        return 0
    }
}
extension ClientDetailViewController{
    
    @objc func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.clientListTableView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.clientListTableView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        self.clientListTableView.contentInset = contentInset
    }
    
    func isAllTextEmpty() -> Bool{
        if (name.text == "" && surname.text == "" && city.text == ""
            && postCode.text == "" && street.text == "" && homeNumber.text == ""
            && phoneNumb.text == "" && phoneNumb2.text == "" && mail.text == ""){
            return true
        } else {
            return false
        }
    }
    
    func emptyFiledAction(){
        
        let alertController = UIAlertController(title: "Musisz uzupełnić przynajmniej jedno pole", message: "", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
}
