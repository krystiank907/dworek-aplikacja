//
//  VMClientList.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 11.04.2018.
//  Copyright © 2018 Krystian Kulawiak. All rights reserved.
//

import Foundation
import RealmSwift

class VMClientList: CurrentClientProtocol{
    
    var clientID: String
    var isReadCleint: Bool
    let items: Results<ClientData>
    let realm: Realm
    
    init(clientID:String = "" ,isReadCleint:Bool = false){
        let syncConfig = SyncConfiguration(user: SyncUser.current!, realmURL: Constants.REALM_URL)
        self.realm = try! Realm(configuration: Realm.Configuration(syncConfiguration: syncConfig))
        self.items = realm.objects(ClientData.self).sorted(byKeyPath: "name", ascending: false)
        self.isReadCleint = isReadCleint
        self.clientID = clientID
        realm.refresh()
    }
    func delateUserAction(index: Int){
        let config = Realm.Configuration(syncConfiguration: SyncConfiguration(user: SyncUser.current!, realmURL: Constants.REALM_URL))
        let realm = try! Realm(configuration: config)
        let item = items[index]
        try! realm.write {
            realm.delete(item)
        }
    }
    
}
