//
//  ClientViewController.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 05.07.2017.
//  Copyright © 2017 Krystian Kulawiak. All rights reserved.
//

import UIKit
import RealmSwift

class ClientViewController: CustomUIViewController,UITextFieldDelegate{ 
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var surname: UITextField!
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var postCode: UITextField!
    @IBOutlet weak var street: UITextField!
    @IBOutlet weak var homeNumber: UITextField!
    @IBOutlet weak var phoneNumb: UITextField!
    @IBOutlet weak var phoneNumb2: UITextField!
    @IBOutlet weak var mail: UITextField!
    
    var viewModel:VMClient!
    
    required init(coder decoder: NSCoder) {
        super.init(coder: decoder)!
    }
    
    init(viewModel:VMClient){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil )
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDelegateTextField()
        setNavigationBar()
        if (viewModel.isDetailClient && viewModel.clientID != "") || viewModel.isReadClient {
            setupClientDetails()
        }
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard(_:)))
        self.view.addGestureRecognizer(tapGesture)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let nextTage=textField.tag+1;
        // Try to find next responder
        let nextResponder=textField.superview?.viewWithTag(nextTage) as UIResponder?
        
        if (nextResponder != nil){
            // Found next responder, so set it.
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        return false // We do not want UITextField to insert line-breaks.
    }

}

extension ClientViewController{
    
    func save(){
        let config = Realm.Configuration(syncConfiguration: SyncConfiguration(user: SyncUser.current!, realmURL: Constants.REALM_URL))
        let realm = try! Realm(configuration: config)
        let client = ClientData()
        client.name = name.text ?? ""
        client.surname = surname.text ?? ""
        client.city = city.text ?? ""
        client.postCode = postCode.text ?? ""
        client.street = street.text ?? ""
        client.homeNumber = homeNumber.text ?? ""
        client.phoneNumb = phoneNumb.text ?? ""
        client.phoneNumb2 = phoneNumb2.text ?? ""
        if viewModel.clientID == ""{
            viewModel.clientID = client.clientId
        }else{
            client.clientId = viewModel.clientID
        }
        try! realm.write {
            realm.add(client, update: true)
        }
    }
    func setNavigationBar() {
        navigationItem.title = "Klient"
        if !viewModel.isDetailClient || viewModel.isReadClient{
            let nextItem = UIBarButtonItem(title: "Dalej", style: .plain, target: self, action: #selector(nextPage))
            let bookmarksItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.bookmarks, target: self, action: #selector(bookmarksAction))
            let rightItem = [nextItem,bookmarksItem]
            navigationItem.rightBarButtonItems = rightItem
        }else{
            let finishItem = UIBarButtonItem(title: "Zapisz", style: .plain, target: self, action: #selector(finish))
            let delateItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.trash, target: self, action: #selector(delateAction))
            let rightItem = [finishItem,delateItem]
            navigationItem.rightBarButtonItems = rightItem
        }
        
    }
    func setupClientDetails(){
        let item = ClientData.currentClient(clientId: viewModel.clientID)
        self.name.text = item!.name
        self.surname.text = item!.surname
        self.city.text = item!.city
        self.postCode.text = item!.postCode
        self.street.text = item!.street
        self.homeNumber.text = item!.homeNumber
        self.phoneNumb.text = item!.phoneNumb
        self.phoneNumb2.text = item!.phoneNumb2
    }
}
extension ClientViewController{
    
    @objc func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    @objc func finish(){
        let isEmpty = isAllTextEmpty()
        if !isEmpty{
            save()
            view.endEditing(true)
            navigationController?.popViewController(animated: true)
        }else {
            emptyFiledAction()
        }
    }
    
    @objc func nextPage(){
        let isEmpty = isAllTextEmpty()
        if !isEmpty{
            save()
            view.endEditing(true)
            let currentClient = ClientData.currentClient(clientId: viewModel.clientID)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "calendar") as! CalendarViewController
            vc.viewModel = VMCalendar(clientID: viewModel.clientID, clientData: currentClient!, isPickDate: true)
            navigationController?.pushViewController(vc, animated: true)
        } else {
            emptyFiledAction()
        }
    }
    
    func emptyFiledAction(){
        
        let alertController = UIAlertController(title: "Musisz uzupełnić przynajmniej jedno pole", message: "", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func delateAction(){
        let isEmpty = isAllTextEmpty()
        if !isEmpty{
            let alertController = UIAlertController(title: "Czy napewno chcesz usunąć użytkownika?", message: "", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Usuń", style: .default, handler: { [unowned self]
                alert -> Void in
                self.viewModel.delateUserAction()
                self.navigationController?.popViewController(animated: true)
            }))
            alertController.addAction(UIAlertAction(title: "Nie usuwaj", style: .cancel, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @objc func bookmarksAction(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "clientList") as! ClientListViewController
        vc.viewModel = VMClientList(isReadCleint: true)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInset
    }
    
    func setDelegateTextField(){
        name.delegate = self
        surname.delegate = self
        city.delegate = self
        postCode.delegate = self
        street.delegate = self
        homeNumber.delegate = self
        phoneNumb.delegate = self
        phoneNumb2.delegate = self
        mail.delegate = self
    }
    
    func isAllTextEmpty() -> Bool{
        if (name.text == "" && surname.text == "" && city.text == ""
            && postCode.text == "" && street.text == "" && homeNumber.text == ""
            && phoneNumb.text == "" && phoneNumb2.text == "" && mail.text == ""){
            return true
        } else {
            return false
        }
    }
    
}
