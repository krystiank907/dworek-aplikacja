//
//  CalendarViewController.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 24.03.2018.
//  Copyright © 2018 Krystian Kulawiak. All rights reserved.
//

import UIKit
import JTAppleCalendar
import RealmSwift

class CalendarViewController: CustomUIViewController {
    
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    @IBOutlet weak var year: UILabel!
    @IBOutlet weak var mounth: UILabel!
    
    let outsideMounthColor = UIColor.lightGray
    let mounthColor = UIColor.black
    var testCalendar = Calendar.current
    let selectedMounthColor = UIColor.white
    let selectedDay = UIColor(displayP3Red: 155.0, green: 201.0, blue: 211.0, alpha: 100.0)
    let formatter = DateFormatter()
    
    var viewModel:VMCalendar!
    
    required init(coder decoder: NSCoder) {
        super.init(coder: decoder)!
    }
    
    init(viewModel:VMCalendar){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil )
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCalendarView()
        calendarView.scrollToDate(Date(), animateScroll: false)
        navigationItem.title = "Kalendarz"
        if viewModel.isPickDate{
            setNavigationBar()
        }
        // Do any additional setup after loading the view.
    
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.delegate = self
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        calendarView.scrollToDate(Date(), animateScroll: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        NotificationCenter.default.removeObserver(self)

        // Dispose of any resources that can be recreated.
    }
    func handleCellTextColor(view: JTAppleCell?, cellState: CellState){
        guard let validCell = view as?  CellView else { return }
        
        formatter.dateFormat = "yyyy MM dd"
        let currentDateString = formatter.string(from: Date())
        let cellStateDateString = formatter.string(from: cellState.date)
        
        if cellState.isSelected && cellState.dateBelongsTo == .thisMonth{
            validCell.dataLabel.textColor = selectedMounthColor
        } else {
            if cellState.dateBelongsTo == .thisMonth{
                validCell.dataLabel.textColor = mounthColor
            } else {
                validCell.dataLabel.text = ""
            }
        }
        if currentDateString ==  cellStateDateString {
            if cellState.isSelected{
                validCell.dataLabel.textColor = selectedMounthColor
                validCell.selectedView.backgroundColor = UIColor.blue
            } else {
                validCell.selectedView.isHidden = false
                validCell.dataLabel.textColor = selectedMounthColor
                validCell.selectedView.backgroundColor = UIColor.darkGray
            }
        }
        
    }
    
    func handleCellSelected(view: JTAppleCell?, cellState: CellState){
        guard let validCell = view as?  CellView else { return }
        if validCell.dataLabel.text != ""{
            if cellState.isSelected && cellState.dateBelongsTo == .thisMonth{
                validCell.selectedView.isHidden = false
                viewModel.currentDate = cellState.date
            } else {
                validCell.selectedView.isHidden = true
            }
        }

    }
    func setupCalendarView(){
        calendarView.minimumLineSpacing = 0
        calendarView.minimumInteritemSpacing = 0
        
        calendarView.visibleDates { (visibleDates) in
            self.setupViewsOfCalendar(form: visibleDates)
        }
        
    }
    
    func setupViewsOfCalendar(form visibleDates: DateSegmentInfo){
        let date = visibleDates.monthDates.first!.date
        self.formatter.dateFormat = "yyyy"
        self.year.text = self.formatter.string(from: date)
        self.formatter.dateFormat = "MMMM"
        let stringDate = changeTranslator(visibleDates: visibleDates)
        self.mounth.text = stringDate
    }
    



}
extension CalendarViewController: JTAppleCalendarViewDataSource{
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        calendarView.scrollToDate(Date(), animateScroll: false)
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        let startDate = formatter.date(from: "2017 01 01")
        let endDate = formatter.date(from: "2100 12 28")
        let firstDayOfWeek: DaysOfWeek = .monday
        
        let parametrs = ConfigurationParameters(startDate: startDate!,
                                                endDate: endDate!,
                                                 firstDayOfWeek: firstDayOfWeek)
        //let parametrs = ConfigurationParameters(startDate: startDate!, endDate: endDate!)
        return parametrs
    }
}

extension CalendarViewController: JTAppleCalendarViewDelegate{
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        // This function should have the same code as the cellForItemAt function
        let myCustomCell = cell as! CellView
        sharedFunctionToConfigureCell(myCustomCell: myCustomCell, cellState: cellState, date: date)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let myCustomCell = calendar.dequeueReusableCell(withReuseIdentifier: "CellView", for: indexPath) as! CellView
        sharedFunctionToConfigureCell(myCustomCell: myCustomCell, cellState: cellState, date: date)
        setupViewsOfCalendar(form: calendar.visibleDates())
        
        return myCustomCell
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        handleCellSelected(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        handleCellSelected(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
    } 

    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        setupViewsOfCalendar(form: visibleDates)
    }
    
    func sharedFunctionToConfigureCell(myCustomCell: CellView, cellState: CellState, date: Date) {
        myCustomCell.dataLabel.text = cellState.text
        handleCellSelected(view: myCustomCell, cellState: cellState)
        handleCellTextColor(view: myCustomCell, cellState: cellState)

    }

}

extension CalendarViewController{
    func changeTranslator(visibleDates: DateSegmentInfo) -> String{
        let date = visibleDates.monthDates.first!.date
        self.formatter.dateFormat = "MMMM"
        let textDate = self.formatter.string(from: date)
        if textDate == "stycznia"{ return "Styczeń" }
        if textDate == "lutego"{ return "Luty" }
        if textDate == "marca"{ return "Marzec" }
        if textDate == "kwietnia"{ return "Kwiecień" }
        if textDate == "maja"{ return "Maj" }
        if textDate == "czerwca"{ return "Czerwiec" }
        if textDate == "lipca"{ return "Lipiec" }
        if textDate == "sierpnia"{ return "Sierpień" }
        if textDate == "września"{ return "Wrzesień" }
        if textDate == "października"{ return "Październik" }
        if textDate == "listopada"{ return "Listopad" }
        if textDate == "grudnia"{ return "Grudzień" }
        else { return textDate }
    }
}

extension CalendarViewController{
    
    @objc func nextPage(){
        viewModel.saveDate()
        let infoData = PartyData.currentPartyInfo(infoId: viewModel.partyId)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "information") as! InformationViewController
        vc.viewModel = VMPartyInforamtion(clientID: viewModel.clientID, dateData: infoData!, clientData: viewModel.clientData)
        view.endEditing(true)
        navigationController?.pushViewController(vc, animated: true)
    }
    func setNavigationBar() {
        let nextItem = UIBarButtonItem(title: "Dalej", style: .plain, target: self, action: #selector(nextPage))
        navigationItem.rightBarButtonItem = nextItem
    }
}




