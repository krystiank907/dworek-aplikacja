//
//  VMCalendar.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 11.04.2018.
//  Copyright © 2018 Krystian Kulawiak. All rights reserved.
//

import Foundation
import RealmSwift

class VMCalendar: CurrentClientProtocol{
    
    let clientID: String
    let clientData: ClientData
    var currentDate = Date()
    var infoId = ""
    var partyId = ""
    var isPickDate: Bool
    
    init(clientID:String, clientData: ClientData , isPickDate: Bool = false){
        self.clientID = clientID
        self.clientData = clientData
        self.isPickDate = isPickDate
    }
    
    func saveDate(){
        let syncConfig = SyncConfiguration(user: SyncUser.current!, realmURL: Constants.REALM_URL)
        let realm: Realm = try! Realm(configuration: Realm.Configuration(syncConfiguration: syncConfig))
        let partyInfo = PartyData()
        try! realm.write {
            partyInfo.partyDate = currentDate
            infoId = partyInfo.partyID
            realm.add(partyInfo, update: true)
            clientData.partyData.append(partyInfo)
            partyId = partyInfo.partyID
        }
        realm.refresh()
        
    }

}
