//
//  DetailTableViewController.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 08.10.2017.
//  Copyright © 2017 Krystian Kulawiak. All rights reserved.
//

import UIKit

class DetailTableViewController: CustomUIViewController,UITableViewDataSource,UITableViewDelegate {
    

    @IBOutlet var tableView: UITableView!
    var parse = JsonParse()
    var dict : [Int:String] = [:]
    var info:String!

    
    override func viewDidLoad() {
        dict = parse.loadMenuFromJson(info: info)
        super.viewDidLoad()
        let backgroundImage = UIImage(named: "2185.png")
        let imageView = UIImageView(image: backgroundImage)
        self.tableView.backgroundView = imageView
        imageView.contentMode = .center
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.delegate = self
    }
    @IBAction func okButton(_ sender: Any) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            _ = storyboard.instantiateViewController(withIdentifier: "menu") as UIViewController
            dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dict.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! DetailViewCell
        cell.nameLabel.text = dict[indexPath.row]
        cell.checkButton.image = cell.checkButtonOff
        for value in selectedInfoFirstDish{
            if value == cell.nameLabel.text!{
                cell.checkButton.image = cell.checkButtonOn
            }
            
        }
        return cell
        
    }
    
    



}
