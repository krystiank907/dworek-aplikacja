//
//  SecondMenuTableViewController.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 09.10.2017.
//  Copyright © 2017 Krystian Kulawiak. All rights reserved.
//

import UIKit

class SecondMenuTableViewController: CustomUIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var tableView: UITableView!
    var parse = JsonParse()
    var dict : [Int:String] = [:]

    var info:String!
    
    override func viewDidLoad() {
        dict = parse.loadMenuFromJson(info: info)
        navigationItem.title = "Dania"
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dict.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SecondMenuTableViewCell
        cell.nameLabel.text = dict[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
    }
    
    ///for showing next detailed screen with the downloaded info
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "detail") as! DetailTableViewController
        vc.info = dict[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }

   
}
