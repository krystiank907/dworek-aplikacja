//
//  DetailViewCell.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 08.10.2017.
//  Copyright © 2017 Krystian Kulawiak. All rights reserved.
//

import UIKit

class DetailViewCell: UITableViewCell {

    @IBOutlet weak var checkButton: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    let checkButtonOn = #imageLiteral(resourceName: "table_check")
    let checkButtonOff = #imageLiteral(resourceName: "table_uncheck")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //checkButton.image = checkButtonOff
        let checkGesture = UITapGestureRecognizer(target: self, action: #selector(DetailViewCell.checkAction(gesture:)))
        checkButton.addGestureRecognizer(checkGesture)
        checkButton.isUserInteractionEnabled = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @objc func checkAction(gesture: UITapGestureRecognizer)
    {
        if (gesture.view as? UIImageView) != nil {
            if checkButton.image == checkButtonOn {
                checkButton.image = checkButtonOff
                selectedFunc(info: false)
            }else{
                checkButton.image = checkButtonOn
                selectedFunc(info: true)
            }
        }
    }
    func selectedFunc(info: Bool){
        if info==true{
            selectedInfoFirstDish.append(nameLabel.text!)
        }
        if info==false{
            for (index, value) in selectedInfoFirstDish.enumerated() {
                if value == nameLabel.text!{
                    selectedInfoFirstDish.remove(at: index)
                }
            }
        }
        
    }
    
    


}
