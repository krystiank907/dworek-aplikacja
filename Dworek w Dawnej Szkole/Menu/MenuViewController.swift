//
//  MenuViewController.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 06.07.2017.
//  Copyright © 2017 Krystian Kulawiak. All rights reserved.
//

import UIKit

class MenuViewController:  CustomUIViewController,UITableViewDataSource,UITableViewDelegate {


    var parse = JsonParse()
    
    @IBOutlet var tableView: UITableView!
    var info:String!
    var dict : [Int:String] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dict = parse.loadMenuFromJson(info: "Menu")
        navigationItem.title = "Menu"
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dict.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MenuTableViewCell
        cell.nameLabel.text = dict[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
    }
    
    ///for showing next detailed screen with the downloaded info
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if dict[indexPath.row] == "Obiad"{
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "menuSecond") as! SecondMenuTableViewController
        vc.info = dict[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "detail") as! DetailTableViewController
            vc.info = dict[indexPath.row]
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
}
