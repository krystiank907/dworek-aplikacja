//
//  EmptySearchTableViewCell.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 17.04.2018.
//  Copyright © 2018 Krystian Kulawiak. All rights reserved.
//

import UIKit

open class EmptySearchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var hintingLabel: UILabel!
    @IBOutlet weak var sugestionLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!

    
    override open func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
    }
    
    override open func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    open func setupCell(_ searchText: String) {
        
        let titleText:NSMutableAttributedString = NSMutableAttributedString(string: "Próba wyszukania frazy: ")
        titleText.append(NSAttributedString(string: "'\(searchText)'"))
        titleText.append(NSAttributedString(string: " \nzakończyła się niepowodzeniem"))
        titleLabel.attributedText = titleText
        titleLabel.numberOfLines = 0
        
        sugestionLabel.text = "Sugestie:"
        hintingLabel.text = "-Sprawdź poprawność wpisanej frazy \n-Spróbuj wpisać mniejszą ilość znaków\n-Spróbuj wpisać słowa kluczowe"
    }
}
