//
//  CustomUIViewController.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 15.04.2018.
//  Copyright © 2018 Krystian Kulawiak. All rights reserved.
//

import Foundation
import UIKit

class CustomUIViewController: UIViewController, UINavigationControllerDelegate{
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.delegate = self
        navigationController?.isNavigationBarHidden = false
        navigationController?.isToolbarHidden = true
    }
    
}
