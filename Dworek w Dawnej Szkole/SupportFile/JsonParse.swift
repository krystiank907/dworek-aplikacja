//
//  JsonParse.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 05.10.2017.
//  Copyright © 2017 Krystian Kulawiak. All rights reserved.
//

import Foundation

class JsonParse {
    
    func loadMenuFromJson(info:String)-> [Int : String]{

     var menuDictionary : [Int : String] = [:]
     var dinnerDictionary : [Int : String] = [:]
     var firstDictionary : [Int : String] = [:]
     var secondDictionary : [Int : String] = [:]
        if let path = Bundle.main.url(forResource: "jsonFile", withExtension: "json") {
            
            do {
                let jsonData = try Data(contentsOf: path, options: .mappedIfSafe)
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? NSDictionary {
                        if let menuArray = jsonResult.value(forKey: "Menu") as? NSArray {
                            for menu in menuArray{
                                if let menuDict = menu as? NSDictionary{
                                    let name = menuDict.value(forKey: "name")
                                    let id = menuDict.value(forKey: "id")
                                    menuDictionary.updateValue(name as! String, forKey: id as! Int)
                                }
                            }
                        }
                        if let menuArray = jsonResult.value(forKey: "Obiad") as? NSArray {
                            for menu in menuArray{
                                if let menuDict = menu as? NSDictionary{
                                    let name = menuDict.value(forKey: "name")
                                    let id = menuDict.value(forKey: "id")
                                    dinnerDictionary.updateValue(name as! String, forKey: id as! Int)
                                }
                            }
                        }
                        if let menuArray = jsonResult.value(forKey: "Pierwsze Danie") as? NSArray {
                            for menu in menuArray{
                                if let menuDict = menu as? NSDictionary{
                                    let name = menuDict.value(forKey: "name")
                                    let id = menuDict.value(forKey: "id")
                                    firstDictionary.updateValue(name as! String, forKey: id as! Int)
                                }
                            }
                        }
                        if let menuArray = jsonResult.value(forKey: "Drugie Danie") as? NSArray {
                            for menu in menuArray{
                                if let menuDict = menu as? NSDictionary{
                                    let name = menuDict.value(forKey: "name")
                                    let id = menuDict.value(forKey: "id")
                                    secondDictionary.updateValue(name as! String, forKey: id as! Int)
                                }
                            }
                        }
                        else {
                            print("Can not found values for key: motors")
                        }
                    } else {
                        print("JSONSerialization failed")
                    }
                } catch let error as NSError {
                    print("Error: \(error)")
                }
            } catch let error as NSError {
                print("Error: \(error)")
            }
        }
        if info == "Menu"{
          return menuDictionary
        }
        if info == "Obiad"{
            return dinnerDictionary
        }
        if info == "Pierwsze Danie"{
            return firstDictionary
        }
        if info == "Drugie Danie"{
            return secondDictionary
        }
        return [0:"error"]
    }
    
}

