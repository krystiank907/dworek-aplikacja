//
//  ClientData.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 24.03.2018.
//  Copyright © 2018 Krystian Kulawiak. All rights reserved.
//

import RealmSwift

class ClientData: Object{
    @objc dynamic var clientId: String = UUID().uuidString
    @objc dynamic var name: String = ""
    @objc dynamic var surname: String = ""
    @objc dynamic var city: String = ""
    @objc dynamic var postCode: String = ""
    @objc dynamic var street: String = ""
    @objc dynamic var homeNumber: String = ""
    @objc dynamic var phoneNumb: String = ""
    @objc dynamic var phoneNumb2: String = ""
    @objc dynamic var mail: String = ""
    let partyData = List<PartyData>()
    
    override static func primaryKey() -> String? {
        return "clientId"
    }
    
    static func currentClient(clientId:String) -> ClientData? {
        
        let config = Realm.Configuration(syncConfiguration: SyncConfiguration(user: SyncUser.current!, realmURL: Constants.REALM_URL))
        let realm = try! Realm(configuration: config)
        let predicate = NSPredicate(format: "clientId == %@",clientId)
        let temp = realm.objects(ClientData.self).filter(predicate)
        if temp.count == 1
        {
            return temp.last
        }
        
        return nil
    }
    static func searchText(searchString: String) -> NSPredicate{
        
        let text = searchString.trimmingCharacters(in: CharacterSet.whitespaces)
        var predicateFormat = "name CONTAINS[c] '\(text)'"
        predicateFormat += "|| surname CONTAINS[c] '\(text)'"
        predicateFormat += "|| city CONTAINS[c] '\(text)'"
        predicateFormat += "|| phoneNumb CONTAINS[c] '\(text)'"
        predicateFormat += "|| phoneNumb2 CONTAINS[c] '\(text)'"
        predicateFormat += "|| mail CONTAINS[c] '\(text)'"
        return NSPredicate(format: predicateFormat)
    }
    
}

