//
//  PartyInfoData.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 11.04.2018.
//  Copyright © 2018 Krystian Kulawiak. All rights reserved.
//

import Foundation
import RealmSwift

class PartyInfoData: Object{
    @objc dynamic var partyInfoId: String = UUID().uuidString
    @objc dynamic var party: String = ""
    @objc dynamic var startTime: String = ""
    @objc dynamic var music: String = ""
    @objc dynamic var amountChild: String = ""
    @objc dynamic var amountPerson: String = ""
    @objc dynamic var otherInformation: String = ""
    
    override static func primaryKey() -> String? {
        return "partyInfoId"
    }
    
}
