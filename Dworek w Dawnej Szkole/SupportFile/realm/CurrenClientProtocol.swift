//
//  CurrenClientProtocol.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 25.03.2018.
//  Copyright © 2018 Krystian Kulawiak. All rights reserved.
//

import Foundation
import RealmSwift

protocol CurrentClientProtocol{
    var clientID: String{ get }
}

