//
//  PartyData.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 28.03.2018.
//  Copyright © 2018 Krystian Kulawiak. All rights reserved.
//

import RealmSwift

class PartyData: Object{
    @objc dynamic var partyID: String = UUID().uuidString
    @objc dynamic var partyDate: Date?
    let partyInfoData = List<PartyInfoData>()
    
    override static func primaryKey() -> String? {
        return "partyID"
    }
    
    static func currentPartyInfo(infoId:String) -> PartyData? {
        let config = Realm.Configuration(syncConfiguration: SyncConfiguration(user: SyncUser.current!, realmURL: Constants.REALM_URL))
        let realm = try! Realm(configuration: config)
        let predicate = NSPredicate(format: "partyID == %@",infoId)
        let temp = realm.objects(PartyData.self).filter(predicate)
        if temp.count == 1
        {
            return temp.last
        }
        
        return nil
    }
    
}
