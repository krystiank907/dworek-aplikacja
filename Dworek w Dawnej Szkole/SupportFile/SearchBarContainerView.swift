//
//  SearchBarContainerView.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 18.04.2018.
//  Copyright © 2018 Krystian Kulawiak. All rights reserved.
//

import Foundation
import UIKit

class SearchBarContainerView: UIView {
    
    let searchBar: UISearchBar
    
    public init(customSearchBar: UISearchBar) {
        searchBar = customSearchBar
        super.init(frame: CGRect.zero)
        
        addSubview(searchBar)
    }
    
    override convenience init(frame: CGRect) {
        self.init(customSearchBar: SearchBar(frame: frame))
        self.frame = frame
    }
    
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        searchBar.frame = bounds
    }
}

class SearchBar: UISearchBar {
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        sizeToFit()
        setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        isTranslucent = true
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override public func setShowsCancelButton(_ showsCancelButton: Bool, animated: Bool) { }
    
}
