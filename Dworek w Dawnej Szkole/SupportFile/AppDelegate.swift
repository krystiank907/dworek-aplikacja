//
//  AppDelegate.swift
//  Dworek w Dawnej Szkole
//
//  Created by Krystian Kulawiak on 23.03.2018.
//  Copyright © 2018 Krystian Kulawiak. All rights reserved.
//

import UIKit
import JTAppleCalendar
import Fabric
import Crashlytics
import RealmSwift
import ChameleonFramework


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        Fabric.with([Crashlytics.self])
        Chameleon.setGlobalThemeUsingPrimaryColor(UIColor(hexString: "47AAB8"), with: UIContentStyle.light)
        // Override point for customization after application launch.
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.loginUser()
        
        self.logUser()
        
        return true
    }
    func logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.sharedInstance().setUserEmail("mlody.k196@gmail.com")
        Crashlytics.sharedInstance().setUserIdentifier("12345")
        Crashlytics.sharedInstance().setUserName("Test User")
        
    }
    func showWindow(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "start")
        //self.window?.rootViewController = initialViewController
        self.window?.makeKeyAndVisible()
        let navigationViewController = UINavigationController(rootViewController: initialViewController)
        UIView.transition(with: window!,
                          duration: 0.3,
                          options: .transitionFlipFromLeft ,
                          animations: { self.window?.rootViewController = navigationViewController } ,
                          completion: nil)
    }
    func loginUser(){
        if let user = SyncUser.current {
            // We have already logged in here!
            user.retrievePermissions { (permission, error) in
                print(permission ?? "", error ?? "")
            }
            let syncConfig = SyncConfiguration(user: SyncUser.current!, realmURL: Constants.REALM_URL)
            let realm = try! Realm(configuration: Realm.Configuration(syncConfiguration: syncConfig))
            try! realm.write {
                realm.autorefresh = true
            }
            showWindow()
        } else {
            let creds = SyncCredentials.nickname("adminUser", isAdmin: true)
            SyncUser.logIn(with: creds,
                           server: Constants.AUTH_URL) { user, error in
                            if let user = user {
                                user.retrievePermissions { (permission, error) in
                                    print(permission ?? "", error ?? "")
                                }
                                let syncConfig = SyncConfiguration(user: SyncUser.current!, realmURL: Constants.REALM_URL)
                                let realm = try! Realm(configuration: Realm.Configuration(syncConfiguration: syncConfig))
                                try! realm.write {
                                    realm.autorefresh = true
                                }
                                
                                self.showWindow()
                                print("User is admin: \(user.isAdmin)")
                            } else if error != nil {
                                fatalError((error?.localizedDescription)!)
                            }
            }
        }
    }
    
    
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}
